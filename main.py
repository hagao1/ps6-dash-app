# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import numpy as np

app = dash.Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
flights = pd.read_csv("flights.csv.gz")
bins = [-np.inf,0,np.inf]
labels = ['take off as scheduled','take off late']
tookoffs = flights[~(np.isnan(flights['dep_delay']))]
dt = tookoffs.groupby(['origin',pd.cut(tookoffs['dep_delay'], bins=bins, labels=labels)]).size().reset_index(name='count')
fig = px.bar(dt, x = 'origin', y = 'count', color="dep_delay", barmode="group", title = 'department delay in each ariport')
fig.update_layout(xaxis_title='original airports', yaxis_title='flight counts')

weather = pd.read_csv("weather.csv.gz")
required = weather[['month', 'day', 'hour', 'humid_EWR','humid_JFK','humid_LGA']].dropna()
required = required.groupby(['month', 'day']).mean()[['humid_EWR','humid_JFK','humid_LGA']]
f = tookoffs.loc[tookoffs['dep_delay'] > 0].groupby(['month', 'day', 'origin']).size().reset_index(name='dcount')
total = tookoffs.groupby(['month', 'day', 'origin']).size().reset_index(name='count')
rate = pd.merge(f, total, how="inner", left_on=['month', 'day', 'origin'], right_on=['month', 'day', 'origin'])
rate['delay percentage'] = rate['dcount']/rate['count']
table = pd.merge(required, rate, how="inner", left_on=['month', 'day'], right_on=['month', 'day'])
table1 = table.loc[table['origin'] == 'EWR'][['month', 'day','humid_EWR','origin','delay percentage']]
table1.rename(columns = {'humid_EWR':'humidity'}, inplace = True)
table2 = table.loc[table['origin'] == 'JFK'][['month', 'day','humid_JFK','origin','delay percentage']]
table2.rename(columns = {'humid_JFK':'humidity'}, inplace = True)
table3 = table.loc[table['origin'] == 'LGA'][['month', 'day','humid_LGA','origin','delay percentage']]
table3.rename(columns = {'humid_LGA':'humidity'}, inplace = True)
table = pd.concat([table1,table2,table3], axis=0)
table.rename(columns = {'origin':'original airports'}, inplace = True)
fig2 = px.scatter(table, x="humidity", y="delay percentage",color="original airports", title ='relationship between delay and humidity')

app.layout = html.Div(children=[
    html.H1(children='ps6 dash app'),

    html.Div(children='''
        This bar graph shows whether the flight is delayed when departing each original airport. From the bar plot, we can know that
        The EWR airports has the most delay while the LGA has the least delay. Also, we can get that, delay is hard to avoid in all the provided airports.
    '''),

    dcc.Graph(
        id='department delay in each ariport',
        figure=fig
    ),

    html.Div(children='''
        This graph shows how humidity affect delay rate. From the graph, we can get that no matter what the airport is, in general, higher humidity leads to higher delay rate.
    '''),

    dcc.Graph(
        id='delay-humidity',
        figure=fig2
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
